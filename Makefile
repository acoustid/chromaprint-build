all:
	echo Run make with some target.

build:
	docker build -t acoustid/chromaprint-build:latest .

upload:
	docker push acoustid/chromaprint-build

.PHONY: all build upload
