FROM atlassian/default-image

RUN apt-get -y update && DEBIAN_FRONTEND=noninteractive apt-get -y install cmake libavcodec-dev libavformat-dev libavutil-dev libavresample-dev libfftw3-dev libgtest-dev libboost-all-dev
